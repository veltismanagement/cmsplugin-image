import os
from setuptools import setup, find_packages

README_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                           'README.rst')

DEPENDENCIES = [
      'Pillow==2.2.1',
      'Django==1.5.5',
      'django-polymorphic==0.5.3',
      'django-cms==2.4.3',
      'django-filer==0.9.5',
      'django-cms-smartsnippets==0.1.21tmr',
      'django-admin-extend>=0.0.1tmr.1',
]

DEPENDENCY_LINKS = [
    'http://github.com/pbs/django-filer@0.9.5#egg=django-filer-0.9.5',
    'git+ssh://bitbucket.com/veltismanagement/django-cms-smartsnippets.git@0.1.21tmr#egg=django-cms-smartsnippets-0.1.21tmr',
    'git+ssh://bitbucket.com/veltismanagement/django-admin-extend.git@0.0.1tmr.1#egg=django-admin-extend-0.0.1tmr.1',
]

setup(
    name='cmsplugin-image',
    version='0.0.6tmr.1',
    description='PBS Image Field type for Django CMS',
    long_description=open(README_PATH, 'r').read(),
    author='PBS Audience Facing Team',
    author_email='tpg-pbs-userfacing@threepillarglobal.com',
    packages=find_packages(),
    include_package_data=True,
    license='BSD License',
    dependency_links=DEPENDENCY_LINKS,
    install_requires=DEPENDENCIES,
)
